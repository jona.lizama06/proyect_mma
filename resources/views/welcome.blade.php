@extends('theme.base')


@section('content')
    <div class="container py-5 text-center">
        <div class="container py-5" style="width: 50%;display: block;margin-left: auto;margin-right: auto;">
            <img src="https://retc.mma.gob.cl/wp-content/uploads/2021/09/logo-retc.png" alt="img">
        </div>
        <a href="{{ route('establecimiento.index') }}" class="btn btn-primary">Establecimientos</a>
    </div>
@endsection
