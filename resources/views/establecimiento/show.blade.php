@extends('theme.base')


@section('content')
    <div class="card text-center w-50 p-3" style="margin: 0 auto;margin-top:20px ">

        <div class="card-header">Detalles de Establecimiento: {{ $establecimiento->id }}</div>
        <div class="card-body">
            <h5 class="card-title">Nombre: {{ $establecimiento->nombre }} </h5>
            <p class="card-text">Dirección: {{ $establecimiento->direction }}</p>
            <p class="card-text">Región: {{ $establecimiento->region }}</p>
            <p class="card-text" >Latitud: {{ $establecimiento->latitud }}</p>
            <p class="card-text" id="longitud">Longitud: {{ $establecimiento->longitud }}</p>
        </div>



        <div class="card" id="mapa" style="width: 300px; height:300px;margin: 0 auto ">Parte del mapa
            <script>
                function initMap() {
                    let latitud = 19.388672;
                    let longitud = -99.174023;

                    coordenadas = {
                        lng: longitud,
                        lat: latitud
                    }

                    generarMapa(coordenadas);
                }


                function generarMapa(coordenadas) {
                    let mapa = new google.maps.Map(document.getElementById('mapa'), {
                        zoom: 12,
                        center: new google.maps.LatLng(coordenadas.lat, coordenadas.lng)
                    });

                    marcador = new google.maps.Marker({
                        map: mapa,
                        draggable: true,
                        position: new google.maps.LatLng(coordenadas.lat, coordenadas.lng)
                    });
                    marcador.addListener('dragend', function(event) {
                        document.getElementById("latitud").value = this.getPosition().lat();
                        document.getElementById("longitud").value = this.getPosition().lng();
                    });

                }
            </script>

            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBD7Rmh9dkpF8wpYcaVA7obVdYyPm8ODPw&callback=initMap">
            </script>
            <br>

            <br>



            <br>
            **
            **
            **
            **
            **
            **
        </div>
    </div>
@endsection
