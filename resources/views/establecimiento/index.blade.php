@extends('theme.base')


@section('content')
    <div class="container py-5 text-center">
        <h1>Listado de Establecimientos</h1>
        <a href="{{ route('establecimiento.create') }}" class="btn btn-primary btn-icon-text mb-2 mb-md-0"><svg
                xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus"
                viewBox="0 0 20 20">
                <path
                    d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
            </svg>
            Crear Establecimiento</a>

        @if (Session::has('mensaje'))
            <div class="alert alert-info my-5">
                {{ Session::get('mensaje') }}
            </div>
        @endif

        <!--hshshshshshshhshsshshshs-->
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Dirección</th>
                    <th scope="col">Región</th>
                    <th scope="col">Latitud</th>
                    <th scope="col">Longitud</th>
                    <th scope="col">Acción</th>
                </tr>
            </thead>
            <tbody>

                @forelse ($establecimientos as $establecimiento)
                    <tr>
                        <td>{{ $establecimiento->id }}</td>
                        <td>{{ $establecimiento->nombre }}</td>
                        <td>{{ $establecimiento->direction }}</td>
                        <td>{{ $establecimiento->region }}</td>
                        <td>{{ $establecimiento->latitud }}</td>
                        <td>{{ $establecimiento->longitud }}</td>
                        <td>
                            <a href="{{ route('establecimiento.show', $establecimiento->id) }}"
                                class="btn btn-success btn-sm"><svg xmlns="http://www.w3.org/2000/svg" width="18"
                                    height="18" fill="currentColor" style="align-items: center" class="bi bi-eye"
                                    viewBox="0 0 16 16">
                                    <path
                                        d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                                    <path
                                        d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                                </svg>Ver</a>
                            <i class="bi bi-brush"></i>
                   

                            <a href="{{ route('establecimiento.edit', $establecimiento->id) }}"
                                class="btn btn-warning btn-sm"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-link" viewBox="0 0 16 16">
                                    <path d="M6.354 5.5H4a3 3 0 0 0 0 6h3a3 3 0 0 0 2.83-4H9c-.086 0-.17.01-.25.031A2 2 0 0 1 7 10.5H4a2 2 0 1 1 0-4h1.535c.218-.376.495-.714.82-1z"/>
                                    <path d="M9 5.5a3 3 0 0 0-2.83 4h1.098A2 2 0 0 1 9 6.5h3a2 2 0 1 1 0 4h-1.535a4.02 4.02 0 0 1-.82 1H12a3 3 0 1 0 0-6H9z"/>
                                  </svg> Editar</a>
                            <form action="{{ route('establecimiento.destroy', $establecimiento) }}" class="d-inline"
                                method="post">
                                @method('DELETE')
                                @csrf
                                <button type="submit" onclick="return confirm('¿Esta seguro de eliminar el registro?')"
                                    class="btn btn-danger btn-sm">Eliminar</button>

                            </form>

                        </td>
                    </tr>
                @empty

                    <tr>
                        <td colspan="3">No existen registros</td>
                    </tr>
                @endforelse
            </tbody>
        </table>

        {{ $establecimientos->links() }}

    </div>
@endsection
