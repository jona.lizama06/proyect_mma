@extends('theme.base')


@section('content')
    <div class="container py-5 text-center">

        @if (isset($establecimiento))
            <h1>Editar Establecimiento</h1>
        @else
            <h1>Crear Establecimiento</h1>
        @endif

        @if (isset($establecimiento))
            <form action="{{ route('establecimiento.update', $establecimiento) }}" method="post"
                class="w-50 p-3 form-horizontal" style="margin: 0 auto ">
                @method('PUT')
            @else
                <form action="{{ route('establecimiento.store') }}" method="post" class="w-50 p-3 form-horizontal"
                    style="margin: 0 auto ">
        @endif



        @csrf
        <!--Nombre-->
        <div class="mb-2">
            <label for="nombre" class="form-label">Nombre</label>
            <input type="text" value="{{ old('nombre') ?? @$establecimiento->nombre }}" class="form-control"
                name="nombre" placeholder="Ingrese el nombre">
            @error('nombre')
                <p class="form-text text-danger ">{{ $message }}</p>
            @enderror
        </div>

        <!--Dirección-->
        <div class="mb-3">
            <label for="direction" class="form-label">Dirección</label>
            <input type="text" value="{{ old('direction') ?? @$establecimiento->direction }}" class="form-control"
                name="direction" placeholder="Ingrese la dirección">
            @error('direction')
                <p class="form-text text-danger ">{{ $message }}</p>
            @enderror
        </div>

        <!--Región-->
        <div class="mb-3">
            <label for="region" class="form-label">Región</label>
            <input type="text" value="{{ old('region') ?? @$establecimiento->region }}" class="form-control"
                name="region" placeholder="Ingrese la región">
            @error('region')
                <p class="form-text text-danger ">{{ $message }}</p>
            @enderror
        </div>

        <!--Latitud-->
        <div class="mb-3">
            <label for="latitud" class="form-label">Latitud</label>
            <input type="number" id="latitud" value="{{ old('latitud') ?? @$establecimiento->latitud }}"
                class="form-control" name="latitud" placeholder="Ingrese la latitud" step="0.000000000000001">
            @error('latitud')
                <p class="form-text text-danger ">{{ $message }}</p>
            @enderror
        </div>


        <!--Longitud-->
        <div class="mb-3">
            <label for="longitud" class="form-label">Longitud</label>
            <input type="number" id="longitud"
                value="{{ old('longitud') ?? @$establecimiento->longitud }}"class="form-control" name="longitud"
                placeholder="Ingrese la longitud" step="0.000000000000001">
            @error('longitud')
                <p class="form-text text-danger ">{{ $message }}</p>
            @enderror
        </div>


        @if (isset($establecimiento))
            <button type="submit" class="btn btn-success">Editar Establecimiento</button>
        @else
            <button type="submit" id="guardar" class="btn btn-success">Guardar Establecimiento</button>
        @endif

        </form>
    </div>


    <div class="card" id="mapa" style="width: 300px; height:300px;margin: 0 auto ">Parte del mapa
     

       <script >

            
            $("#guardar").click(function() {
                let lat = $("#latitud").val();
                let lng = $("#longitud").val();

                coordenadas = {
                    lng: lng,
                    lat: lat
                }

                generarMapa(coordenadas);

            });

            function initMap() {
                let latitud = -33.4708251007103;
                let longitud = -70.65136876967257;

                coordenadas = {
                    lng: longitud,
                    lat: latitud
                }

                generarMapa(coordenadas);
            }


            function generarMapa(coordenadas) {
                let mapa = new google.maps.Map(document.getElementById('mapa'), {
                    zoom: 12,
                    center: new google.maps.LatLng(coordenadas.lat, coordenadas.lng)
                });

                marcador = new google.maps.Marker({
                    map: mapa,
                    draggable: true,
                    position: new google.maps.LatLng(coordenadas.lat, coordenadas.lng)
                });
                marcador.addListener('dragend', function(event) {
                    document.getElementById("latitud").value = this.getPosition().lat();
                    document.getElementById("longitud").value = this.getPosition().lng();
                });

            }
        </script>

        <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMrOTQePpXvu0FlI3Gm4zqfO1uDhZIEkc&region=CL&libraries=places&callback=initMap">
        </script>
    @endsection
