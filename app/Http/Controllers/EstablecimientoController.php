<?php

namespace App\Http\Controllers;

use App\Models\Establecimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EstablecimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        //

        $establecimientos = Establecimiento::paginate(4);
        return view('establecimiento.index')->with('establecimientos', $establecimientos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('establecimiento.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // definiremos reglas de validación en nuestro formulario
        $request->validate([
            'nombre' => 'required|max:30',
            'direction' => 'required|max:30',
            'region' => 'required|max:20',
            'latitud' => 'required',
            'longitud' => 'required',
        ]);

        $establecimiento = Establecimiento::create($request->only('nombre', 'direction', 'region', 'latitud', 'longitud'));
        Session::flash('mensaje', 'Registro creado con exito');
        return redirect()->route('establecimiento.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $establecimiento = Establecimiento::find($id);

        return view('establecimiento.show')->with('establecimiento',$establecimiento);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Establecimiento $establecimiento)
    {
        //

        return view('establecimiento.form')->with('establecimiento', $establecimiento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Establecimiento $establecimiento)
    {
        //

        // definiremos reglas de validación en nuestro formulario
        $request->validate([
            'nombre' => 'required|max:30',
            'direction' => 'required|max:30',
            'region' => 'required|max:20',
            'latitud' => 'required',
            'longitud' => 'required',
        ]);

        $establecimiento->nombre = $request['nombre'];
        $establecimiento->direction = $request['direction'];
        $establecimiento->región = $request['region'];
        $establecimiento->latitud = $request['latitud'];
        $establecimiento->longitud = $request['longitud'];
        $establecimiento->save();


        Session::flash('mensaje', 'Registro editado con exito');
        return redirect()->route('establecimiento.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Establecimiento $establecimiento)
    {
        //
        $establecimiento->delete();
        Session::flash('mensaje', 'Registro eliminado con exito!');
        return redirect()->route('establecimiento.index');
    }
}
